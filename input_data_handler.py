import copy
import csv
import os
import datetime
import time

import csv
import pandas as pd
import math

from matplotlib import pyplot as plt
from sklearn.cluster import DBSCAN
import numpy as np


class InputDataHandlerIVASchmetz:
    def __init__(self, input_folder_path, output_folder_path):
        self.input_folder_path = input_folder_path
        self.output_folder_path = output_folder_path

        self.df = pd.DataFrame()
        self.data_frames = []

    def save_as_xlsx(self):
        print("Writing Excel File ...")
        self.df.to_excel(self.output_folder_path + "/output.xlsx", sheet_name='data', index=False)
        print("Done with the Excel File")

    def load_folder(self):
        # Read all files in the input directory
        files_input = [
            f for f in os.listdir(self.input_folder_path)
            if os.path.isfile(os.path.join(self.input_folder_path, f))
        ]

        # Collect the data of all files in the input folder and merge them into one dataframe
        roots_pump_dict = {}
        print("Loading pandas data frame")
        self.df = pd.concat([pd.read_csv(os.path.join(self.input_folder_path, file), skiprows=1, usecols=[0, 4, 7, 15, 16]) for file in files_input[0:]], ignore_index=True)
        print("Pandas data frame loaded with " + str(len(self.df.index)))

        # Transform to custom data frames
        print("Transform to custom data frames")
        self.data_frames.clear()
        process_start = None
        process_data = []
        process_t = []
        process_p = []
        data_frames_disqualified = []
        hold_sampling = True
        for d_i in self.df.index:
            datetime_struct = pd.to_datetime(self.df.iloc[d_i, 0], format='%Y-%m-%d %H:%M:%S.%f')
            p = self.df.iloc[d_i, 1]
            #vacuum_no = self.df.iloc[d_i, 2]
            backing_pump = self.df.iloc[d_i, 3]
            event_frame_no = self.df.iloc[d_i, 4]
            roots_pump = self.df.iloc[d_i, 2]
            if not math.isnan(event_frame_no):
                # Store last process
                if len(process_t) > 0:
                    # Preprocess the data of the last event and store it if it is not disqualified
                    result_t, result_p = self.clean_vacuum_process(process_t, process_p)
                    process_data.append(result_t)
                    process_data.append(result_p)
                    if result_t is not None:
                        self.data_frames.append(copy.deepcopy(process_data))
                    else:
                        data_frames_disqualified.append(copy.deepcopy(process_data))

                # Prepare next process
                process_data.clear()
                process_t = []
                process_p = []
                process_start = datetime_struct
                event_frame_no = int(event_frame_no)
                #vacuum_no = int(vacuum_no)
                #process_data.append((event_frame_no, vacuum_no))
                meta_data = []
                meta_data.append(event_frame_no)
                process_data.append(meta_data)
                # The sampling is put on hold until the backing pump is activated
                hold_sampling = True

            release_hold_sampling = False
            # Resume sampling when the backing pump is activated
            if not math.isnan(backing_pump):
                if backing_pump == 1.0:
                    release_hold_sampling = True

            if not hold_sampling and not math.isnan(p):
                if process_start is not None:
                    diff = datetime_struct - process_start
                    seconds_since_process_start = diff.total_seconds()
                    if seconds_since_process_start < 0:
                        print("Error")
                        continue
                    process_t.append(seconds_since_process_start)
                    process_p.append(p)

            if release_hold_sampling:
                hold_sampling = False

            if not math.isnan(roots_pump):
                if roots_pump == 1.0:
                    if math.isnan(p):
                        print("p unavailable at roots pump start")
                    else:
                        v = int(p)
                        if v not in roots_pump_dict:
                            roots_pump_dict[v] = 1
                        else:
                            roots_pump_dict[v] += 1

        print("Processed. There is a total of " + str(len(self.data_frames)) + " entries and " + str(len(data_frames_disqualified)) + " entries were disqualified")

        printIt = True
        if printIt:
            # print(roots_pump_dict)
            print(dict(sorted(roots_pump_dict.items(), key=lambda item: item[1])))

            # Plot the data
            plt.figure(figsize=(8, 5))  # Optional: Set the figure size
            plt.scatter(list(roots_pump_dict.keys()), list(roots_pump_dict.values()), color='b', label='Data')

            # Add labels and title
            plt.xlabel('Keys')
            plt.ylabel('Values')
            plt.title('Plot of Dictionary')
            plt.legend()

            # Show the grid
            plt.grid(True)

            # Display the plot
            plt.show()

            print("done")
            plt.figure(figsize=(16, 12))
            for i, data_frame in enumerate(self.data_frames):
            #for i, data_frame in enumerate(data_frames_disqualified):
                if i < 400:
                    plt.plot(data_frame[1], data_frame[2], marker='o', label=f"Trajectory {i}")
            # Add labels and legend
            plt.xlabel('X')
            plt.ylabel('Y')
            plt.title('Trajectories')
            #plt.legend()
            plt.grid(True)
            plt.show()

    def load_vacuum_processes(self):
        row_timestamp = 0
        row_value_vacuum = 4
        row_roots_pump = 7
        row_backing_pump = 15
        row_event_frame = 16

        # Read all files in the input directory
        files_input = [
            f for f in os.listdir(self.input_folder_path)
            if os.path.isfile(os.path.join(self.input_folder_path, f))
        ]

        # Collect the data of all files in the input folder and merge them into one dataframe
        roots_pump_dict = {}
        print("Loading pandas data frame")
        self.df = pd.concat([pd.read_csv(os.path.join(self.input_folder_path, file), skiprows=1, usecols=[row_timestamp, row_value_vacuum, row_roots_pump, row_backing_pump, row_event_frame]) for file in files_input[0:]], ignore_index=True)
        print("Pandas data frame loaded with " + str(len(self.df.index)))

        # Transform to custom data frames
        print("Transform to custom data frames")
        transformation_start_time = time.time()
        self.data_frames.clear()
        process_start = None
        process_data = []
        process_t = []
        process_p = []
        data_frames_disqualified = []
        hold_sampling = True
        for d_i in self.df.index:
            datetime_struct = pd.to_datetime(self.df.iloc[d_i, 0], format='%Y-%m-%d %H:%M:%S.%f')
            p = self.df.iloc[d_i, 1]
            roots_pump = self.df.iloc[d_i, 2]
            backing_pump = self.df.iloc[d_i, 3]
            event_frame_no = self.df.iloc[d_i, 4]

            if not math.isnan(event_frame_no):
                # Store last process
                if len(process_t) > 0:
                    print("process has " + str(len(process_t)) + " samples")
                    # Preprocess the data of the last event and store it if it is not disqualified
                    result_t, result_p = self.clean_vacuum_process(process_t, process_p)
                    process_data.append(result_t)
                    process_data.append(result_p)
                    if result_t is not None:
                        self.data_frames.append(copy.deepcopy(process_data))
                    else:
                        data_frames_disqualified.append(copy.deepcopy(process_data))

                # Prepare next process
                process_data.clear()
                process_t = []
                process_p = []
                process_start = datetime_struct
                event_frame_no = int(event_frame_no)
                meta_data = []
                meta_data.append(event_frame_no)
                process_data.append(meta_data)
                # The sampling is put on hold until the backing pump is activated
                hold_sampling = True

            # cutoff after 800 samples
            #if len(process_t) >= 800:
            #    continue

            release_hold_sampling = False
            # Resume sampling when the backing pump is activated
            if not math.isnan(backing_pump):
                if backing_pump == 1.0:
                    release_hold_sampling = True

            if not hold_sampling and not math.isnan(p):
                if process_start is not None:
                    diff = datetime_struct - process_start
                    seconds_since_process_start = diff.total_seconds()
                    if seconds_since_process_start < 0:
                        print("Error")
                        continue
                    process_t.append(seconds_since_process_start)
                    process_p.append(p)

            if release_hold_sampling:
                hold_sampling = False

            if not math.isnan(roots_pump):
                if roots_pump == 1.0:
                    if math.isnan(p):
                        print("p unavailable at roots pump start")
                    else:
                        v = int(p)
                        if v not in roots_pump_dict:
                            roots_pump_dict[v] = 1
                        else:
                            roots_pump_dict[v] += 1

        transformation_end_time = time.time()

        print("Processed. There is a total of " + str(len(self.data_frames)) + " entries and " + str(len(data_frames_disqualified)) + " entries were disqualified")
        print("Transformation took " + str(transformation_end_time - transformation_start_time) + "s")

        printIt = True
        if printIt:
            # print(roots_pump_dict)
            print(dict(sorted(roots_pump_dict.items(), key=lambda item: item[1])))

            # Plot the data
            plt.figure(figsize=(8, 5))  # Optional: Set the figure size
            plt.scatter(list(roots_pump_dict.keys()), list(roots_pump_dict.values()), color='b', label='Data')

            # Add labels and title
            plt.xlabel('Keys')
            plt.ylabel('Values')
            plt.title('Plot of Dictionary')
            plt.legend()

            # Show the grid
            plt.grid(True)

            # Display the plot
            plt.show()

            print("done")
            plt.figure(figsize=(16, 12))
            for i, data_frame in enumerate(self.data_frames):
            #for i, data_frame in enumerate(data_frames_disqualified):
                if i < 400:
                    plt.plot(data_frame[1], data_frame[2], marker='o', label=f"Trajectory {i}")
            # Add labels and legend
            plt.xlabel('X')
            plt.ylabel('Y')
            plt.title('Trajectories')
            #plt.legend()
            plt.grid(True)
            plt.show()

    def clean_vacuum_process(self, process_t, process_p):
        # Parameters
        monotony_threshold = 0.4
        difference_percent = 0.02

        # Resample and check conditions
        resampled_t = []
        resampled_p = []

        # Check beginning
        # In the loval data, the first few data points must be excluded, as they don't belong to the process and are lower than the maximum
        # Set the start to the most right heighest point in the first half
        # Search the first half of the list for this (it is more efficient and prevents problems if data values at the end are high)
        # Also, check for unusual high step lengths (>10)
        #max_value_index = max(enumerate(process_p[:(len(process_p) // 2)]), key=lambda x: x[1])[0]
        list_half_index = len(process_p) // 2
        max_value_index = 0
        max_value = 0
        for i in range(list_half_index):
            #if process_p[i] >= max_value and (process_t[i + 1] - process_t[i]) <= 10:
            # the seocnd condition means, the time gap is not too high and last one ensures there is a high slope (Although this could be improved)
            if process_p[i] >= max_value and (process_t[i + 1] - process_t[i]) <= 10 and (process_p[i + 5] < 800):
                max_value_index = i
                max_value = process_p[i]

        start_index = max_value_index

        # If the star sample has changed, adjust all consecutive samples relative to it
        adjust_time = process_t[start_index]
        if adjust_time > 0:
            for i in range(start_index, len(process_t)):
                process_t[i] = process_t[i] - adjust_time


        for i in range(start_index, len(process_t)):
            # Check same value
            if len(resampled_t) > 0:
                diff_percent = abs(process_p[i] - resampled_p[-1]) / resampled_p[-1]

                # Sample will only be stored when there is enough change in value
                if diff_percent < difference_percent:
                    continue

                # Check monotony
                if diff_percent > monotony_threshold:
                    #print("Disqualified due to: Monotony")
                    #return None, None
                    continue

                # Cutoff if the process is too long
                if process_t[i] > 800:
                    break

            resampled_t.append(process_t[i])
            resampled_p.append(process_p[i])

        # Check length, there should be a minimum of 10 samples in the event
        if len(resampled_t) < 10:
            print("Disqualified due to: Not enough samples")
            return None, None


        # Check end value
        if resampled_p[-1] > 1:
            print("Disqualified due to: Wrong end value")
            return None, None


        # ToDo: Include processes, where only the last bit goes up again
        # ToDo: Make a statistic
        # ToDo: Plot cleaned curves
        # ToDo: x spacing?

        return resampled_t, resampled_p

    def identify_processes(self):
        # Get a rough idea by finding set temperatures
        dict_set_temperatures = {}
        dict_set_temperatures_sequences = {}

        x = []
        y = []

        '''
        event_frame_start = 0
        event_frame_set_temperatures = []
        for d_i in self.df.index:
            event_frame_no = self.df.iloc[d_i, 9]
            if not math.isnan(event_frame_no):
                time_string_raw = self.df.iloc[d_i, 0]
                datetime_struct = time.strptime(time_string_raw, '%Y-%m-%d %H:%M:%S.%f')
                total_seconds = int(datetime.timedelta(hours=datetime_struct.tm_hour, minutes=datetime_struct.tm_min, seconds=datetime_struct.tm_sec).total_seconds())
                event_frame_start = total_seconds

                # Store last sequence
                if len(event_frame_set_temperatures) > 0:
                    tuple_key = tuple(event_frame_set_temperatures)
                    dict_set_temperatures_sequences[tuple_key] = dict_set_temperatures_sequences.get(tuple_key, 0) + 1

                event_frame_set_temperatures.clear()

            key = self.df.iloc[d_i, 8]
            if not math.isnan(key):
                dict_set_temperatures[key] = dict_set_temperatures.get(key, 0) + 1
                if len(event_frame_set_temperatures) == 0 or event_frame_set_temperatures[-1] != key:
                    event_frame_set_temperatures.append(key)

                time_string_raw = self.df.iloc[d_i, 0]
                datetime_struct = time.strptime(time_string_raw, '%Y-%m-%d %H:%M:%S.%f')
                total_seconds = int(datetime.timedelta(hours=datetime_struct.tm_hour, minutes=datetime_struct.tm_min, seconds=datetime_struct.tm_sec).total_seconds())

                if total_seconds - event_frame_start < 0:
                    print("Error")
                    continue

                x.append(total_seconds - event_frame_start)
                y.append(key)
        print(dict_set_temperatures)
        print(dict_set_temperatures_sequences)
        #plt.scatter(scatter_x, scatter_y)
        #plt.show()
        '''

        dict_process_set_temperatures_sequences = {}
        process_start = None
        process_set_temperatures = []
        process_x = []
        process_y = []
        zero_based = False
        for d_i in self.df.index:
            vacuum_no = self.df.iloc[d_i, 3]
            if not math.isnan(vacuum_no):
                vacuum_no = int(vacuum_no)
            event_frame_no = self.df.iloc[d_i, 4]
            datetime_struct = pd.to_datetime(self.df.iloc[d_i, 0], format='%Y-%m-%d %H:%M:%S.%f')
            if not math.isnan(event_frame_no):
                # Store last sequence
                #if (vacuum_no == 0 or (vacuum_no == 1 and not zero_based)) and len(process_set_temperatures) > 0:
                #if (vacuum_no == 0 or (vacuum_no == 1 and not zero_based)) and len(process_set_temperatures) > 0:
                if vacuum_no <= 1 and len(process_set_temperatures) > 0:
                    tuple_key = tuple(process_set_temperatures)
                    dict_process_set_temperatures_sequences[tuple_key] = dict_process_set_temperatures_sequences.get(tuple_key, 0) + 1
                    if tuple_key == (0.0, 400.0, 580.0):
                        x.extend(process_x)
                        y.extend(process_y)

                    process_start = datetime_struct
                    process_set_temperatures.clear()
                    process_x = []
                    process_y = []
                    zero_based = vacuum_no == 0

            key = self.df.iloc[d_i, 8]
            if not math.isnan(key):
                if len(process_set_temperatures) == 0 or process_set_temperatures[-1] != key:
                    process_set_temperatures.append(key)

                    if process_start is not None:
                        diff = datetime_struct - process_start
                        seconds_since_process_start = diff.total_seconds()
                        if seconds_since_process_start < 0:
                            print("Error")
                            continue

                        # track values for a specific process
                        if seconds_since_process_start < 50000:
                            process_x.append(seconds_since_process_start)
                            process_y.append(key)
        #print(dict_process_set_temperatures_sequences)
        sorted_data = sorted(dict_process_set_temperatures_sequences.items(), key=lambda item: item[1], reverse=True)
        print("Sorted Dictionary (by values):")
        for key, value in sorted_data:
            print(f"{value}: {key}")

        plt.scatter(x, y)
        plt.show()




        '''
        # Apply DBSCAN clustering
        dbscan = DBSCAN(eps=100.0, min_samples=2)
        data = np.array(list(zip(x, y)))
        labels = dbscan.fit_predict(data)

        # Visualize the results
        n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
        print(f'Number of clusters found: {n_clusters}')
        plt.scatter(x, y, c=labels, cmap='viridis')
        plt.xlabel('Time (x)')
        plt.ylabel('Values (y)')
        plt.title('DBSCAN Clustering')
        plt.show()
        '''

    def create_input_data_old(self):
        # Read all files
        files_input = os.listdir(self.input_folder_path)

        number_of_input_files = len(files_input)

        # Define the dtypes of the columns
        cols = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        dtype_dict = {
            '0': pd.StringDtype(),
            '1': float,
            '2': float,
            '3': bool,
            '4': bool,
            '5': float,
            '6': float,
            '7': float,
            '8': float,
            '9': int
        }

        # Build a dataframe
        df = pd.concat([pd.read_csv(os.path.join(self.input_folder_path, file), skiprows=1, names=cols) for file in files_input[0:]], ignore_index=True)
        df = df[df.columns[[0, 1, 9]]]

        # Save it as a csv file
        #df.to_csv(os.path.join(self.output_folder_path, "results.csv"), index=False)

        all_event_frames_data = []

        new_event_frame_index = 1
        last_event_frame = -1
        event_frame_start_total_seconds = -1
        current_event_frame_data = []
        for i in df.index:
            time_string_raw = df.iloc[i, 0]
            datetime_struct = time.strptime(time_string_raw, '%Y-%m-%d %H:%M:%S.%f')
            total_seconds = int(datetime.timedelta(hours=datetime_struct.tm_hour, minutes=datetime_struct.tm_min, seconds=datetime_struct.tm_sec).total_seconds())

            event_frame_raw = df.iloc[i, 2]
            if math.isnan(event_frame_raw):
                event_frame = last_event_frame
            else:
                if last_event_frame > -1:
                    all_event_frames_data.append(current_event_frame_data.copy())
                current_event_frame_data.clear()
                event_frame = new_event_frame_index
                new_event_frame_index += 1
                event_frame_start_total_seconds = total_seconds

            # Get relative time
            seconds_this_tuple = total_seconds - event_frame_start_total_seconds
            if seconds_this_tuple < 0:
                continue

            pressure = df.iloc[i, 1]
            if math.isnan(pressure):
                continue

            current_event_frame_data.append((seconds_this_tuple, pressure))
            last_event_frame = event_frame

        all_event_frames_data.append(current_event_frame_data)

        # Sort out some invalid frames
        valid_event_frames_data = []
        for event_frame_data in all_event_frames_data:
            resampled_data = []

            # Check start value
            start_index = 0
            start_threshold = 10
            adjust_seconds = 0
            for u in range(len(event_frame_data) - 1):
                if abs(event_frame_data[u + 1][1] - event_frame_data[u][1]) < start_threshold:
                    start_index += 1
                    adjust_seconds = event_frame_data[u + 1][0]
                else:
                    break

            # Infuse synthetic start
            real_start_pressure = event_frame_data[start_index][1]
            t_estimation = math.sqrt((1013.25-real_start_pressure)/5)
            resampled_data.append((0, 1013.25))
            adjust_seconds -= round(t_estimation)

            # Resample and check conditions
            monotony_threshold = 2
            difference_percent = 0.05
            resampled_data.append((event_frame_data[start_index][0] - adjust_seconds, event_frame_data[start_index][1]))
            for u in range(start_index + 1, len(event_frame_data)):
                # Check same value
                if abs(event_frame_data[u][1] - event_frame_data[u - 1][1]) / event_frame_data[u - 1][1] < difference_percent:
                    continue

                # Check monotony
                if event_frame_data[u][1] < event_frame_data[u-1][1] + monotony_threshold:
                    resampled_data_tuple = (event_frame_data[u][0] - adjust_seconds, event_frame_data[u][1])

                    # Check cutoff
                    if resampled_data_tuple[1] < 1.5 or resampled_data_tuple[0] > 800:
                        break

                    resampled_data.append(resampled_data_tuple)
                else:
                    break

            # Check length
            if len(resampled_data) < 10:
                continue

            # Check end value
            if resampled_data[-1][1] > 5:
                continue

            # if the event frame reached this line, it is accepted for further processing
            valid_event_frames_data.append(resampled_data)

        print(str(len(valid_event_frames_data)) + " valid event frames from originally " + str(len(all_event_frames_data)))

        return valid_event_frames_data

    def save_as_csv(self):
        # ToDo: Make file with: id, pump_change_id, no_entries, t_n, value_n
        print("Saving preprocessed data in a csv")
        with open(self.output_folder_path + '/preprocessedData.csv', 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for idx, df in enumerate(self.data_frames):
                row = [idx]  # Start with the ID of the DataFrame

                row.extend(df[0])
                row.append(len(df[1]))
                row.extend(df[1])
                row.extend(df[2])

                writer.writerow(row)  # Write the row to the CSV file
        print("Saving in csv done")

    def save_vacuum_processes_as_csv(self, split_at_roots_pump):
        # ToDo: Make file with: id, pump_change_id, no_entries, t_n, value_n
        print("Saving preprocessed data in a csv")
        with open(self.output_folder_path + '/preprocessed_vacuum_processes.csv', 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for idx, df in enumerate(self.data_frames):
                row = [idx]  # Start with the ID of the DataFrame

                row.extend(df[0])
                row.append(len(df[1]))
                row.extend(df[1])
                row.extend(df[2])

                writer.writerow(row)  # Write the row to the CSV file
        print("Saving in csv done")

    def read_vacuum_processes_from_csv(self):
        self.data_frames.clear()
        with open(self.output_folder_path + "/preprocessed_vacuum_processes.csv", mode='r') as file:
            reader = csv.reader(file)
            for row in reader:
                # First element is the ID, remaining elements are data
                idx = row[0]
                data_frame = []

                data_frame.append([row[1]])
                no_entries = int(row[2])
                data_frame.append(list(map(float, row[3:3+no_entries])))
                data_frame.append(list(map(float, row[3+no_entries:3+no_entries+1+no_entries])))

                self.data_frames.append(data_frame)



    def plot_vacuum_processes(self, overlayed_process = None):
        plt.figure(figsize=(16, 12))
        for i, data_frame in enumerate(self.data_frames):
            # for i, data_frame in enumerate(data_frames_disqualified):
            if i < 800 and 800 < data_frame[2][0] < 900:
                if overlayed_process is None:
                    plt.plot(data_frame[1], data_frame[2], marker='o', label=f"Trajectory {i}")
                else:
                    plt.plot(data_frame[1], data_frame[2], marker='o', label=f"Trajectory {i}", color='k')

        # Plot overlayed process
        if overlayed_process is not None:
            plt.plot(overlayed_process[0], overlayed_process[1], marker='o', label=f"Trajectory {i}", color='r')

        # Add labels and legend
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.title('Trajectories')
        # plt.legend()
        plt.grid(True)
        plt.show()






