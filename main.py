import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import keras
from keras.src.utils import losses_utils

from input_data_handler import InputDataHandlerIVASchmetz
from rnn_model import RNNModel
from simulation_model_vacuum import SimulationModelVacuum


def make_rnn_model(number_of_inputs):
    model = RNNModel()
    model.add_layer(keras.layers.InputLayer((None, 2), 1, ragged=True))
    model.add_layer(keras.layers.GRU(128, return_sequences=True, return_state=True, activation='relu'))
    model.add_layer(keras.layers.Dense(units=128, activation='relu'))
    model.add_layer(keras.layers.Dense(units=64, activation='relu'))
    model.add_layer(keras.layers.Dense(units=64, activation='relu'))
    model.add_layer(keras.layers.Dense(units=1))
    return model


def make_training_set():
    pass


def experiment_with_simulation():
    pass


def experiment_rnn_only():
    pass


idh = InputDataHandlerIVASchmetz("io/input", "io/output")
#input_data = idh.load_folder()
#idh.load_vacuum_processes()
#idh.save_vacuum_processes_as_csv()
#idh.identify_processes()
#idh.save_as_xlsx()
idh.read_vacuum_processes_from_csv()


# Create and parametrize the simulation model
p_start = 1013.25
p_start = 850
V = 10
dt = 0.1  # [s]
s = 0.33
l = 0.0002
t_end = 800
steps = int(t_end / dt)

simulation_model = SimulationModelVacuum()
simulation_model.set_parameters(p_start=p_start, volume=V, pump_speed=s, leakage_rate=l)
simulation_model.initialize()

# Data for plotting
time_axis = np.arange(0.0, t_end, dt)
simulated_data = np.zeros((steps, 1))
inputs = np.zeros((steps, 5))
p = p_start
t = 0
for x in range(steps):
    simulated_data[x][0] = p
    inputs[x] = [p, t, s, l, V]
    p = simulation_model.simulation_step(dt)
    t += dt
#idh.plot_vacuum_processes((time_axis, simulated_data))

# Create the RNN model
model = make_rnn_model(2)

# Should the training work on the original data or relative to the simulation results
relative_training = False
use_simulation = True

# Prepare the training batch
x_train_data = []
y_train_data = []
number_of_training_processes = 125
for i in range(min(number_of_training_processes, len(idh.data_frames))):
    input_entry = idh.data_frames[i]
    p_start = input_entry[2][0]
    #roots_pump_start = 0
    t_x = input_entry[1]
    p_x_sim = simulation_model.simulate_for_timepoints(t_x)

    x_data = [(float(t), float(p_start), float(p_x_sim[i])) for i, t in enumerate(t_x)]
    y_data = [y for y in input_entry[2]]

    # Make relative
    if relative_training:
        for u in range(len(y_data)):
            y_data[u] -= simulated_data[x_data[u]][0]

    x_train_data.append(x_data)
    y_train_data.append(y_data)

# for each simulated point, it should predict the "difference to reality"
# inputs: simulated_value, *here should be more [t?start?rootspumpe?]*
# target: real_value
# outputs: adjustment

# Create the actual train set and adjust the dimensions as needed
x_train = tf.ragged.constant(x_train_data, dtype=float, ragged_rank=2)
print(x_train.shape)
y_train = tf.ragged.constant(y_train_data, dtype=float)
print(y_train.shape)

#x_train = tf.expand_dims(x_train, 2)
#x_train = tf.reshape(x_train, (-1, None, 2))
#y_train = tf.expand_dims(y_train, 2)

train_dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train))
train_dataset = train_dataset.batch(batch_size=1, drop_remainder=True)

# Print details and example
"""
print("dataset:")
for input_example_batch, target_example_batch in train_dataset.take(1):
    print("sample of train_set:")
    print(input_example_batch)
    print(target_example_batch)
"""

"""
# Directory where the checkpoints will be saved
checkpoint_dir = './training_checkpoints'
# Name of the checkpoint files
checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt_{epoch}.weights.h5")

checkpoint_callback = keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_prefix,
    save_weights_only=True)
"""

# Define the loss function
loss = keras.losses.MeanSquaredError(
    reduction=losses_utils.ReductionV2.AUTO,
    name='mean_squared_error'
)

# Define the optimizer
optimizer = keras.optimizers.Adam(
    learning_rate=0.001
)

# Compile and build the model
model.compile(optimizer=optimizer, loss=loss, metrics=[keras.metrics.MeanAbsoluteError()])
model.build(input_shape=(1, None, 3))
model.summary()

# Train
print("Begin of training")
EPOCHS = 25
history = model.fit(train_dataset, epochs=EPOCHS)
print("Training finished")

# Test
test_input = np.ones((1, 1, 3))
test_input[0, 0, 0] = 0
test_input[0, 0, 1] = 850
test_input[0, 0, 2] = 850
result = model(test_input)

'''
# Find errors
check = False
if check:
    for i in range(len(input_data)):
        input_entry = input_data[i]

        x_data = [x[0] for x in input_entry]
        y_data = [y[1] for y in input_entry]

        # Make relative
        if relative_training:
            for u in range(len(y_data)):
                y_data[u] -= simulated_data[x_data[u]][0]

        x_train_data.append(x_data)
        y_train_data.append(y_data)
'''

# 1: Example Prediction
all_results = []
s = None
h = None
p_x_sim = simulation_model.simulate_for_timepoints(list(range(0, 801, 1)))
for i in range(800):
    test_input[0, 0, 0] = i
    test_input[0, 0, 1] = 850
    test_input[0, 0, 2] = p_x_sim[i]
    r, h, s = model(test_input, hidden=h, states=s, return_state=True)
    all_results.append(r[0, 0, 0])


# Plot some results
fig, ax = plt.subplots()
ax.set(xlabel='time (s)', ylabel='pressure (mbar)',
       title='Evacuation  Process')
ax.grid()

plt.plot(simulated_data[0::10], 'black')
plt.plot(all_results, 'r')
plt.show()



# 2: Relative Comparison to Point 1

# 3: Relative Comparison to Point 2

# 4, Combined only: Training performance






# 1) Record values
# 2) Get parameters of what to do
# 3) Make a prediction
# 4) Measure the distance (by DTW?)
# 5) Record it over time!! just with this, this makes sense




