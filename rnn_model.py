import tensorflow as tf
import keras


class RNNModel(keras.Model):
    def __init__(self):
        super().__init__()
        self.custom_layers = []

    def add_layer(self, layer):
        self.custom_layers.append(layer)

    def call(self, inputs, hidden=None, states=None, return_state=False, training=False):
        x = inputs
        for layer in self.custom_layers:
            if isinstance(layer, keras.layers.GRU) or isinstance(layer, keras.layers.LSTM):
                if states is None:
                    states = layer.get_initial_state(x)
                x, states = layer(x, initial_state=states, training=training)
            else:
                x = layer(x, training=training)

        if return_state:
            return x, hidden, states
        else:
            return x

    # Not implemented correctly
    @tf.function
    def sample_one_step(self, inputs, states=None):
        predicted_logits, states = self.call(inputs=inputs, states=states, return_state=True)
        predicted_logits = predicted_logits[:, -1, :]
        return 0
