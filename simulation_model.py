from abc import abstractmethod


class SimulationModel:
    @abstractmethod
    def set_parameters(self, *args, **kwargs):
        pass

    @abstractmethod
    def initialize(self, *args, **kwargs):
        pass

    @abstractmethod
    def simulation_step(self, dt):
        pass

    def simulate_for_timepoints(self, t_x):
        max_step = 0.1
        self.initialize()
        simulated_data = []
        t = 0.0
        for i_t in range(len(t_x)):
            t_next = t_x[i_t]
            # i chose <= to include the first simulation step with probably dt = 0 also in the loop
            while t <= t_next:
                dt = min(max_step, t_next - t)
                t += dt
                value = self.simulation_step(dt)
                if t == t_next:
                    simulated_data.append(value)
                    break
        return simulated_data
