import math

from simulation_model import SimulationModel


class SimulationModelVacuum(SimulationModel):
    def __init__(self):
        self.V = None
        self.s = None
        self.l = None
        self.p_last = None
        self.p_start = None

    def set_parameters(self, p_start, volume, pump_speed, leakage_rate):
        self.V = volume
        self.s = pump_speed
        self.l = leakage_rate
        self.p_start = p_start

    def initialize(self):
        self.p_last = self.p_start

    def simulation_step(self, dt):
        exponent_of_decrease = 1.1 # 1 is linear, 2 quadratic, etc
        exponent_multiplier = 0.85 # 1 means at x = 1 it is zero, below one means zero is reached beyon x = 1, which means the curve is damped

        s_p = self.s * math.exp(1 * (-1 + (self.p_last / 1013.25)))
        s_p = self.s * (1 - exponent_multiplier * math.pow(1 - (self.p_last / 1013.25), exponent_of_decrease))
        exponent = -self.s * dt / self.V
        exponent = -s_p * dt / self.V
        pump_term = self.p_last * math.exp(exponent)
        #pump_term = self.p_last * math.exp(exponent)
        leakage_term = self.l * math.sqrt(1013.25 - self.p_last) * dt
        #leakage_term = self.l * math.pow(1013.25 - self.p_last, 1.1) * dt
        result = pump_term + leakage_term
        self.p_last = result
        return result
